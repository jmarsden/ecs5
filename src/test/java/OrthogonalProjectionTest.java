/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import co.cata.ecs.renderer.OrthogonalProjection;
import java.nio.FloatBuffer;
import co.cata.math.Matrix4f;
import co.cata.utils.MatrixPrinter;
import junit.framework.TestCase;
import org.lwjgl.BufferUtils;

/**
 *
 * @author jmarsden
 */
public class OrthogonalProjectionTest extends TestCase {

    public OrthogonalProjectionTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of setMatrix2D method, of class OrthogonalProjection.
     */
    public void testSetMatrix2D() {
        System.out.println("setMatrix2D");
        Matrix4f matrix = Matrix4f.identity();
        OrthogonalProjection instance = new OrthogonalProjection(0, 800, 600, 0, 0, 1);
        
        
        Matrix4f expResult = null;
        Matrix4f result = instance.setMatrix2D(matrix);
        System.out.println(result);
        result.print(4, 4);
    }

    /**
     * Test of setMatrix method, of class OrthogonalProjection.
     */
    public void testSetMatrix() {
        System.out.println("setMatrix");

        FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        Matrix4f referenceMatrix = Matrix4f.identity();
        createReferenceOrthogonalMatrix(referenceMatrix);
        referenceMatrix.load(matrix44Buffer);
        matrix44Buffer.flip();
        
        System.out.println("Reference Array:\r\n" + matrix44Buffer.toString());
        System.out.println(MatrixPrinter.matrix2String(referenceMatrix));
        
        Matrix4f ecsMatrix = Matrix4f.identity();
        OrthogonalProjection instance = new OrthogonalProjection(0, 8, 0, 6, 0, 1);
        instance.load(ecsMatrix);

        //FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        matrix44Buffer.clear();
        ecsMatrix.loadColumnMajor(matrix44Buffer);
        matrix44Buffer.flip();

        System.out.println("ECS Array:\r\n" + matrix44Buffer.toString());
        System.out.println(MatrixPrinter.matrix2String(ecsMatrix));
        
        //assertEquals(expResult, result);
    }

    public Matrix4f createReferenceOrthogonalMatrix(Matrix4f projectionMatrix) {
        float left = 0;
        float right = 8;
        float top = 0;
        float bottom = 6;
        float near = 0;
        float far = 1;

        projectionMatrix.setData(0, 0, 2f / (right - left));
        projectionMatrix.setData(1, 1, 2f / (top - bottom));
        projectionMatrix.setData(2, 2, -2f / (far - near));
        projectionMatrix.setData(3, 3, 1f);

        projectionMatrix.setData(0, 3, -((right + left) / (right - left)));
        projectionMatrix.setData(1, 3, -((top + bottom) / (top - bottom)));
        projectionMatrix.setData(2, 3, -((far + near) / (far - near)));

        return projectionMatrix;
    }
}
