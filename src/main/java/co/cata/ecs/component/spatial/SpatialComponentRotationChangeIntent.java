/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.component.spatial;

import co.cata.ecs.engine.Component;
import co.cata.ecs.engine.ComponentIntent;

public class SpatialComponentRotationChangeIntent extends ComponentIntent {

	float rotation;
	float rotationDelta;
	
	public SpatialComponentRotationChangeIntent(Component source, float rotation, float rotationDelta) {
		super(source);
		this.rotation = rotation;
		this.rotationDelta = rotationDelta;
	}

	/**
	 * @return the rotation
	 */
	public float getRotation() {
		return rotation;
	}

	/**
	 * @param rotation the rotation to set
	 */
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	/**
	 * @return the rotationDelta
	 */
	public float getRotationDelta() {
		return rotationDelta;
	}

	/**
	 * @param rotationDelta the rotationDelta to set
	 */
	public void setRotationDelta(float rotationDelta) {
		this.rotationDelta = rotationDelta;
	}
}
