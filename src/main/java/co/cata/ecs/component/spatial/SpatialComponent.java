/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.component.spatial;

import co.cata.ecs.engine.Component;
import co.cata.ecs.engine.GameObject;
import co.cata.ecs.engine.IntentRegistry;
import co.cata.ecs.renderer.BoundingVolume;
import co.cata.math.Matrix3f;
import co.cata.math.Matrix4f;
import co.cata.math.Transformation;
import co.cata.math.Vector3f;

/**
 *
 * @author jmarsden
 */
public class SpatialComponent extends Component {

//    public enum SPATIAL_STATE {
//        CURRENT,
//        UPDATE
//    }
//    
//    public SPATIAL_STATE state;
    public Transformation local;

    public Transformation world;

    //public boolean worldIsCurrent;
    // public BoundingVolume objectBound;
    //public BoundingVolume worldBound;
    //public boolean wouldBoundIsCurrent;
    /**
     * Intent Topics
     */
    public final int xChangeTopic;

    public final int yChangeTopic;

    public final int zChangeTopic;

    public final int moveTopic;

    public SpatialComponent() {
        super();

        IntentRegistry ir = IntentRegistry.lookupSingleton();

        local = new Transformation();
        world = new Transformation();

        xChangeTopic = ir.lookupIntent(SpatialComponentLocalTXChangeIntent.class);
        yChangeTopic = ir.lookupIntent(SpatialComponentLocalTYChangeIntent.class);
        zChangeTopic = ir.lookupIntent(SpatialComponentLocalTZChangeIntent.class);
        moveTopic = ir.lookupIntent(SpatialComponentLocalMoveIntent.class);
    }

    public void setRotation(Matrix3f r) {
        local.rotation = r;
    }

    public void setTranslation(float x, float y) {
        setTranslation(x, y, 0f);
    }

    public void setTranslation(float x, float y, float z) {
        boolean xFlag = x != local.translation.x;
        boolean yFlag = y != local.translation.y;
        boolean zFlag = z != local.translation.z;
        if(xFlag) {
            onXTranslationChange(x, x - local.translation.x);
            local.translation.x = x;
        }
        if(yFlag) {
            onYTranslationChange(y, y - local.translation.y);
            local.translation.y = y;
        }
        if(zFlag) {
            onZTranslationChange(z, z - local.translation.z);
            local.translation.z = z;
        }
        if(xFlag || yFlag || zFlag) {
            onTranslationChange(x, y, z);
        }
    }

    public void setScale(float s) {
        setScale(s, s, s);
    }

    public void setScale(float x, float y, float z) {
        local.scale.setData(x, y, z);
    }

//    public void checkUpdateState() {
//        if (state == SPATIAL_STATE.UPDATE) {
//            // Update Local Bound
//            // Update World Transform
//            // Update World Bound 
//        } 
//    }
//    
//    public void updateLocalBound() {
//        if(gameObject == null) {
//           objectBound.reset(); 
//           return;
//        } 
//        if(gameObject.geometry != null) {
//            // Find Game Objects Local Bound
//        }
//        if(!gameObject.children.isEmpty()) {
//            for(GameObject child: gameObject.children) {
//                
//            }
//        }
//    }
//    
//    public void UpdateWorldTransform() {
//        
//    }
//    
//    public void updateWorldBound() {
//        
//    }
    public void crossoverLocalToWorld() {
        world.store(local);
    }

    public void combineWorldWithParent(SpatialComponent parentSpatial) {
        world.concatinate(parentSpatial.world);
    }

    /**
     * Intent Events
     */
    
    public void onXTranslationChange(float x, float xDelta) {
        if(activeIntentTopicRecipients(xChangeTopic)) {
            sendIntent(new SpatialComponentLocalTXChangeIntent(this, x, xDelta));
        }
    }

    public void onYTranslationChange(float y, float yDelta) {
        if(activeIntentTopicRecipients(yChangeTopic)) {
            sendIntent(new SpatialComponentLocalTYChangeIntent(this, y, yDelta));
        }
    }

    public void onZTranslationChange(float y, float yDelta) {
        if(activeIntentTopicRecipients(zChangeTopic)) {
            sendIntent(new SpatialComponentLocalTZChangeIntent(this, y, yDelta));
        }
    }

    public void onTranslationChange(float x, float y, float z) {
        if(activeIntentTopicRecipients(moveTopic)) {
            sendIntent(new SpatialComponentLocalMoveIntent(this, x, y, z));
        }
    }

    public void load(Matrix4f matrix) {
        world.load(matrix);
    }
}