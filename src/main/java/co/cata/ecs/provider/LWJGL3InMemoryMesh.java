/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.renderer.Mesh;
import co.cata.ecs.renderer.Shader;
import co.cata.graphics.Vertex;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;

/**
 *
 * @author J.W.Marsden
 */
public class LWJGL3InMemoryMesh extends Mesh {

    public Vertex[] verticies;
    public short[] indicies;
    public int vertexHandle;
    public int indiciesHandle;
    public int indicesCount;
    public boolean initialized;

    public LWJGL3InMemoryMesh(Vertex[] verticies, short[] indicies) {
        vertexHandle = -1;
        indiciesHandle = -1;
        indicesCount = -1;
        this.verticies = verticies;
        this.indicies = indicies;
        initialized = false;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void init() {
        initialized = true;
    }

    public boolean isLoaded() {
        if (vertexHandle != -1 && indiciesHandle != -1) {
            return true;
        } else {
            return false;
        }
    }

    public void load() {
        vertexHandle = GL15.glGenBuffers();
        indiciesHandle = GL15.glGenBuffers();

        ByteBuffer verticesByteBuffer = BufferUtils.createByteBuffer(verticies.length * Vertex.getByteCount(Vertex.POSITION | Vertex.NORMAL | Vertex.COLOR));

        FloatBuffer verticesFloatBuffer = verticesByteBuffer.asFloatBuffer();
        for (int i = 0; i < verticies.length; i++) {
            verticies[i].get(Vertex.POSITION | Vertex.NORMAL | Vertex.COLOR, verticesFloatBuffer);
        }
        verticesFloatBuffer.rewind();

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexHandle);

        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesFloatBuffer, GL15.GL_STREAM_DRAW);

        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        indicesCount = indicies.length;
        ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(indicesCount * 2);

        ShortBuffer indiciesShortBuffer = indicesBuffer.asShortBuffer();
        indiciesShortBuffer.put(indicies);
        indiciesShortBuffer.rewind();

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indiciesHandle);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indiciesShortBuffer, GL15.GL_STREAM_DRAW);

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    public void enable() {
    }

    public void disable() {
    }
    
    public void enable(Shader shader) {
    }

    public void disable(Shader shader) {
    }

    public void unLoad() {
        GL15.glDeleteBuffers(indiciesHandle);

        GL15.glDeleteBuffers(vertexHandle);

        vertexHandle = -1;
        indiciesHandle = -1;
    }

    public void release() {
        initialized = false;
    }

    @Override
    public int getIndicesCount() {
        return indicesCount;
    }
}
