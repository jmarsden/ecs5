/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.renderer.Mesh;
import co.cata.ecs.renderer.Shader;
import co.cata.graphics.Vertex;
import co.cata.utils.ArrayPrinter;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author jmarsden
 */
public class LWJGL3InMemoryMeshVAO extends Mesh {

    public Vertex[] verticies;
    public short[] indicies;
    public int vertaxArrayHandle;
    public int vertexHandle;
    public int indiciesHandle;
    public int indicesCount;
    public boolean initialized;
    public boolean enabled;

    public LWJGL3InMemoryMeshVAO(Vertex[] verticies, short[] indicies) {
        vertaxArrayHandle = -1;
        vertexHandle = -1;
        indiciesHandle = -1;
        indicesCount = -1;
        this.verticies = verticies;
        this.indicies = indicies;
        initialized = false;
        enabled = false;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void init() {
        initialized = true;
    }

    public boolean isLoaded() {
        if (vertaxArrayHandle != -1 && vertexHandle != -1 && indiciesHandle != -1) {
            return true;
        } else {
            return false;
        }
    }

    public void load() {
        FloatBuffer verticesFloatBuffer = convert(verticies);
        //System.out.println("ECS Vertex Buffer:\t" + ArrayPrinter.arrayToString(verticesFloatBuffer));
        verticesFloatBuffer.rewind();

        vertaxArrayHandle = GL30.glGenVertexArrays();

        GL30.glBindVertexArray(vertaxArrayHandle);

        vertexHandle = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexHandle);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesFloatBuffer, GL15.GL_STREAM_DRAW);

//        if (shader.hasAttribute("in_Position")) {
//            GL20.glVertexAttribPointer(shader.getAttributeLocation("in_Position"), Vertex.POSITION_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.POSITION_BYTE_OFFSET);
//        }
//        if (shader.hasAttribute("in_Normal")) {
//            GL20.glVertexAttribPointer(shader.getAttributeLocation("in_Normal"), Vertex.NORMAL_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.NORMAL_BYTE_OFFSET);
//        }
//        if (shader.hasAttribute("in_Color")) {
//            GL20.glVertexAttribPointer(shader.getAttributeLocation("in_Color"), Vertex.COLOR_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.COLOR_BYTE_OFFSET);
//        }
//        if (shader.hasAttribute("in_TextureCoord")) {
//            GL20.glVertexAttribPointer(shader.getAttributeLocation("in_TextureCoord"), Vertex.TEXTURE_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.TEXTURE_BYTE_OFFSET);
//        }
        indicesCount = indicies.length;
        ByteBuffer indiciesBuffer = BufferUtils.createByteBuffer(indicesCount * 2);
        ShortBuffer indiciesShortBuffer = indiciesBuffer.asShortBuffer();
        indiciesShortBuffer.put(indicies);
        indiciesShortBuffer.flip();
        //System.out.println("ECS Indices Buffer:\t" + ArrayPrinter.arrayToString(indiciesShortBuffer));
        indiciesShortBuffer.rewind();

        indiciesHandle = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indiciesHandle);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indiciesShortBuffer, GL15.GL_STATIC_DRAW);

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL30.glBindVertexArray(0);
    }

    private FloatBuffer convert(Vertex[] vertices) {
        ByteBuffer verticesByteBuffer = BufferUtils.createByteBuffer(vertices.length * Vertex.STRIDE);
        FloatBuffer verticesFloatBuffer = verticesByteBuffer.asFloatBuffer();
        for (int i = 0; i < vertices.length; i++) {
            verticesFloatBuffer.put(vertices[i].getElements());
        }
        verticesFloatBuffer.flip();
        return verticesFloatBuffer;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void enable() {

    }

    public void enable(Shader shader) {
        GL30.glBindVertexArray(vertaxArrayHandle);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexHandle);

        if (shader.hasAttribute("in_Position")) {
            GL20.glVertexAttribPointer(shader.getAttributeLocation("in_Position"), Vertex.POSITION_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.POSITION_BYTE_OFFSET);
        }
        if (shader.hasAttribute("in_Normal")) {
            GL20.glVertexAttribPointer(shader.getAttributeLocation("in_Normal"), Vertex.NORMAL_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.NORMAL_BYTE_OFFSET);
        }
        if (shader.hasAttribute("in_Color")) {
            GL20.glVertexAttribPointer(shader.getAttributeLocation("in_Color"), Vertex.COLOR_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.COLOR_BYTE_OFFSET);
        }
        if (shader.hasAttribute("in_TextureCoord")) {
            GL20.glVertexAttribPointer(shader.getAttributeLocation("in_TextureCoord"), Vertex.TEXTURE_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.TEXTURE_BYTE_OFFSET);
        }

        if (shader.hasAttribute("in_Position")) {
            int location = shader.getAttributeLocation("in_Position");
            GL20.glEnableVertexAttribArray(location);
        }
        if (shader.hasAttribute("in_Normal")) {
            int location = shader.getAttributeLocation("in_Normal");
            GL20.glEnableVertexAttribArray(location);
        }
        if (shader.hasAttribute("in_Color")) {
            int location = shader.getAttributeLocation("in_Color");
            GL20.glEnableVertexAttribArray(location);
        }
        if (shader.hasAttribute("in_TextureCoord")) {
            int location = shader.getAttributeLocation("in_TextureCoord");
            GL20.glEnableVertexAttribArray(location);
        }
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indiciesHandle);
        enabled = true;
    }

    public void disable() {

    }

    public void disable(Shader shader) {

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        if (shader.hasAttribute("in_TextureCoord")) {
            GL20.glDisableVertexAttribArray(shader.getAttributeLocation("in_TextureCoord"));
        }
        if (shader.hasAttribute("in_Color")) {
            GL20.glDisableVertexAttribArray(shader.getAttributeLocation("in_Color"));
        }
        if (shader.hasAttribute("in_Normal")) {
            GL20.glDisableVertexAttribArray(shader.getAttributeLocation("in_Normal"));
        }
        if (shader.hasAttribute("in_Position")) {
            GL20.glDisableVertexAttribArray(shader.getAttributeLocation("in_Position"));
        }
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL30.glBindVertexArray(0);
        enabled = false;
    }

    public void unLoad() {
    }

    public void release() {
        initialized = false;
    }

    @Override
    public String dumpState() {
        StringBuilder state = new StringBuilder();
        state.append(getClass().getCanonicalName()).append('\n');
        state.append("isIntialised[").append(isInitialized()).append("] ").append("isLoaded[").append(isLoaded()).append("] ").append("isEnabled[").append(isEnabled()).append("]").append('\n');
        state.append("vao[").append(vertaxArrayHandle).append("] ").append("vbo[").append(vertexHandle).append("] ").append("indicies[").append(indiciesHandle).append("]").append('\n');
        state.append("Verticies[").append(verticies.length).append("]").append('\n');
        for (int i = 0; i < verticies.length; i++) {
            state.append("[").append(i).append("] ").append(verticies[i]).append('\n');
        }
        state.append("Indicies[").append(indicesCount).append("]").append('\n');
        for (int i = 0; i < indicies.length; i++) {
            state.append("[").append(i).append("] ").append(indicies[i]).append('\n');
        }

        return state.toString();
    }

    @Override
    public int getIndicesCount() {
        return indicesCount;
    }
}
