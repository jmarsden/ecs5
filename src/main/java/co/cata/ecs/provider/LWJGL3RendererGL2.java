/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.provider;

import co.cata.ecs.engine.GameObject;
import co.cata.ecs.renderer.GLSLVersion;
import static co.cata.ecs.renderer.GLSLVersion.GLSL110;
import static co.cata.ecs.renderer.GLSLVersion.GLSL120;
import static co.cata.ecs.renderer.GLSLVersion.GLSL130;
import static co.cata.ecs.renderer.GLSLVersion.GLSL140;
import static co.cata.ecs.renderer.GLSLVersion.GLSL430;
import co.cata.ecs.renderer.Mesh;
import co.cata.ecs.renderer.Shader;
import co.cata.ecs.renderer.ShaderDefaults;
import co.cata.ecs.renderer.Texture;
import co.cata.math.Transformation;
import java.net.URL;
import org.lwjgl.opengl.GL11;

public class LWJGL3RendererGL2 extends LWJGL3Renderer {

    public LWJGL3RendererGL2(LWJGL3Display display) {
        super(display);
    }

    @Override
    public void init() {
        GL11.glClearColor(backgroundColor.getRed() / 255F, backgroundColor.getGreen() / 255F, backgroundColor.getBlue() / 255F, 1.0f);

        switch (getGLSLVersion()) {
            case GLSL110:
                throw new RuntimeException("Dont Support " + GLSLVersion.GLSL110);
            case GLSL120:
                throw new RuntimeException("Dont Support " + GLSLVersion.GLSL120);
            case GLSL130:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_130, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_130);
                }
                break;
            case GLSL140:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_140, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_140);
                }
                break;
            case GLSL150:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }
                break;
            case GLSL330:
            case GLSL400:
            case GLSL410:
            case GLSL420:
            case GLSL430:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }
                break;

            default:
                if (getGLSLVersion().getVersion() > GLSL430.getVersion()) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }

                throw new RuntimeException("Dont Support " + getGLSLVersion());
        }

        if (!defaultShader.isInitialized()) {
            defaultShader.init();
        }
        if (!defaultShader.isLoaded()) {
            defaultShader.load();
        }
        if (defaultShader.inError()) {
            throw new RuntimeException("Default Shader No Compile: " + defaultShader.getVertexCompileError() + " " + defaultShader.getFragmentCompileError() + " " + defaultShader.getLinkError());
        }

        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);
    }

    public StringLWJGLShader createShader(String vertexSource, String fragmentSource) {
        return new StringLWJGLShader(vertexSource, fragmentSource);
    }

    public StringLWJGLShader createShader(URL vertexURL, URL fragmentURL) {
        return new StringLWJGLShader(vertexURL, fragmentURL);
    }

    public void releaseShader(Shader shader) {
        shader.release();
    }

    public StringLWJGLShader createDefaultShader(String vertexSource, String fragmentSource) {
        if (defaultShader != null) {
            defaultShader.release();
        }
        defaultShader = createShader(vertexSource, fragmentSource);
        return defaultShader;
    }

    public StringLWJGLShader createDefaultShader(URL vertexURL, URL fragmentURL) {
        if (defaultShader != null) {
            defaultShader.release();
        }
        defaultShader = createShader(vertexURL, fragmentURL);
        return defaultShader;
    }

    public StringLWJGLShader getDefaultShader() {
        return defaultShader;
    }

    public void completeRender() {

        if (!defaultShader.isLoaded()) {
            defaultShader.load();
        }

        if (skipRender) {
            batch.clear();
            lock = false;
            return;
        }

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

        projection.load(projectionMatrix);

        camera.load(viewMatrix);

        for (GameObject gameObject : batch.queue) {

            if (gameObject.geometry == null || !gameObject.geometry.isValid()) {
                continue;
            }
            if (gameObject.spatial == null) {
                continue;
            }

            Shader shader = getDefaultShader();
            Texture texture = gameObject.geometry.texture;
            Mesh mesh = gameObject.geometry.mesh;

            Transformation transformation = gameObject.getWorld();
            transformation.load(modelMatrix);

	    if(texture != null) {
		texture.enable();
	    }
            
            {
                shader.enable();
                {
                    shader.loadUniform(shader.getUniformLocation("projectionMatrix"), projectionMatrix);
                    shader.loadUniform(shader.getUniformLocation("viewMatrix"), viewMatrix);
                    shader.loadUniform(shader.getUniformLocation("modelMatrix"), modelMatrix);

                    mesh.enable(shader);
                    GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.getIndicesCount(), GL11.GL_UNSIGNED_SHORT, 0);
                    mesh.disable(shader);
                }
                shader.disable();
            }
	    
	    if(texture != null) {
		texture.disable();
	    }
	}

        batch.clear();
        lock = false;
    }
}
