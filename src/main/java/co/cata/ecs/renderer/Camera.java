/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

import co.cata.math.Matrix3f;
import co.cata.math.Matrix4f;
import co.cata.math.Vector3f;

public abstract class Camera {

    protected boolean dirty;
 
    public float[] mat4x4;
    
    public Camera() {

        mat4x4 = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
	
        load(mat4x4);
        dirty = false;
    }

    public float[] load(float[] buffer) {

        if (buffer == null || buffer.length != 16) {
            throw new NullPointerException("Need a better error here");
        }

        buffer[0] = mat4x4[0];
        buffer[1] = mat4x4[1];
        buffer[2] = mat4x4[2];
        buffer[3] = mat4x4[3];
        buffer[4] = mat4x4[4];
        buffer[5] = mat4x4[5];
        buffer[6] = mat4x4[6];
        buffer[7] = mat4x4[7];
        buffer[8] = mat4x4[8];
        buffer[9] = mat4x4[9];
        buffer[10] = mat4x4[10];
        buffer[11] = mat4x4[11];
        buffer[12] = mat4x4[12];
        buffer[13] = mat4x4[13];
        buffer[14] = mat4x4[14];
        buffer[15] = mat4x4[15];

        return mat4x4;
    }

    public void load(Matrix4f matrix) {
        matrix.mat4x4[0] = mat4x4[0];
        matrix.mat4x4[1] = mat4x4[1];
        matrix.mat4x4[2] = mat4x4[2];
        matrix.mat4x4[3] = mat4x4[3];
        matrix.mat4x4[4] = mat4x4[4];
        matrix.mat4x4[5] = mat4x4[5];
        matrix.mat4x4[6] = mat4x4[6];
        matrix.mat4x4[7] = mat4x4[7];
        matrix.mat4x4[8] = mat4x4[8];
        matrix.mat4x4[9] = mat4x4[9];
        matrix.mat4x4[10] = mat4x4[10];
        matrix.mat4x4[11] = mat4x4[11];
        matrix.mat4x4[12] = mat4x4[12];
        matrix.mat4x4[13] = mat4x4[13];
        matrix.mat4x4[14] = mat4x4[14];
        matrix.mat4x4[15] = mat4x4[15];
    }
    
    public abstract boolean validate();
    
    public abstract void setTranslation(float x, float y, float z);
    
    public abstract void lookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ);
    
    public void lookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ) {
	lookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, 0f, 1f, 0f);
    }
}
