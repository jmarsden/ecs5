/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.cata.ecs.renderer;

import co.cata.math.Vector3f;

/**
 *
 * @author JOHNMARSDEN
 */
public class WorldCamera extends Camera {

    public Vector3f eye;
    public Vector3f center;
    
    public Vector3f forward;
    public Vector3f right;
    public Vector3f up;
    
    private Vector3f n;
    private Vector3f u;
    private Vector3f v;
    
    public WorldCamera() {
	eye = new Vector3f(0,0,0);
	center = new Vector3f(0,0,5);
        forward = new Vector3f(0,0,1);
        right = new Vector3f(1,0,0);
        up = new Vector3f(0,1,0);
	
	n = new Vector3f(0,0,0);
	u = new Vector3f(0,0,0);
	v = new Vector3f(0,0,0);
    }

    public boolean validate() {
	// Check alignment of direction vectors
	if(forward.dot(right) != 0) {
	    return false;
	} else if(forward.dot(up) != 0) {
	    return false;
	} else if(up.dot(right) != 0) {
	    return false;
	} else if (!forward.cross(up).equals(right)) {
	    return false;
	}
	return true;
    }
    
    
    @Override
    public void setTranslation(float eyeX, float eyeY, float eyeZ) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void lookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
	eye.setData(eyeX, eyeY, eyeZ);
	center.setData(centerX, centerY, centerZ);
	up.setData(upX, upY, upZ);
	
	eye.subtract(forward, n);
	n.normalise();
	
	up.cross(n, u);
	u.normalise();
	
	n.cross(u, v);
	v.normalise();	
	
	float tx = - u.dot(eye);
	float ty = - v.dot(eye);
	float tz = - n.dot(eye);

	mat4x4[0] = u.x;
        mat4x4[1] = v.x;
        mat4x4[2] = n.x;
        mat4x4[3] = 0;
        mat4x4[4] = u.y;
        mat4x4[5] = v.y;
        mat4x4[6] = n.y;
        mat4x4[7] = 0;
        mat4x4[8] = u.z;
        mat4x4[9] = v.z;
        mat4x4[10] = n.z;
        mat4x4[11] = 0;
        mat4x4[12] = tx;
        mat4x4[13] = ty;
        mat4x4[14] = tz;
        mat4x4[15] = 1;

    }    

}
