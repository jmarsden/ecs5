/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

public class Extensions {

    String[] extensionArray;
    
    public Extensions(String extensions) {
        extensionArray = extensions.split(" ");
    }
    
    public boolean supportsExtension(String name) {
        for(String extension: extensionArray) {
            if(extension.equals(name)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        StringBuilder extensionBuilder = new StringBuilder();
        extensionBuilder.append('[').append(extensionArray.length).append(']').append(' ');
        for(String extension: extensionArray) {
            extensionBuilder.append(extension).append(' ');
        }
        return extensionBuilder.toString();
    }
}
