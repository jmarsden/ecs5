/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

public enum GLSLVersion {

    GLSL110(1.1),
    GLSL120(1.2),
    GLSL130(1.3),
    GLSL140(1.4),
    GLSL150(1.5),
    GLSL330(3.3),
    GLSL400(4.0),
    GLSL410(4.1),
    GLSL420(4.2),
    GLSL430(4.3),
    GLSL440(4.4),
    GLSLNEW(999.0),
    GLSLNONE(0.0);
    double version;

    GLSLVersion(double version) {
        this.version = version;
    }

    public double getVersion() {
        return version;
    }

    @Override
    public String toString() {
        return "" + version;
    }
}
