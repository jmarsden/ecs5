/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

import co.cata.math.Matrix4f;
import java.nio.FloatBuffer;

public class OrthogonalProjection extends Projection {

    float left;
    float right;
    float bottom;
    float top;
    float near;
    float far;

    /**
     * Default Constructor
     *
     * @param left The left clipping plane
     * @param right The right clipping plane
     * @param top The top clipping plane
     * @param bottom The bottom clipping plane
     * @param near The near clipping plane
     * @param far The far clipping plane
     */
    public OrthogonalProjection(float left, float right, float top, float bottom, float near, float far) {
        this.left = left;
        this.right = right;
        this.bottom = bottom;
        this.top = top;
        this.near = near;
        this.far = far;
        OrthogonalProjection.this.load(mat4x4);
    }

    public OrthogonalProjection(float left, float right, float top, float bottom) {
        this.left = left;
        this.right = right;
        this.bottom = bottom;
        this.top = top;
        this.near = 0;
        this.far = 1;
        OrthogonalProjection.this.load(mat4x4);
    }

    /**
     * Sets this matrix to an orthographic projection matrix with the origin at
     * (x,y) extending by width and height. The near plane is set to 0, the far
     * plane is set to 1.
     *
     * @param matrix
     * @return This matrix for chaining
     */
    public Matrix4f setMatrix2D(Matrix4f matrix) {
        return OrthogonalProjection.this.load(matrix);
    }

    @Override
    public final float[] load(float[] mat4x4) {
        float x_orth = 2f / (right - left);
        float y_orth = 2f / (top - bottom);
        float z_orth = -2f / (far - near);

        float tx = -((right + left) / (right - left));
        float ty = -((top + bottom) / (top - bottom));
        float tz = -((far + near) / (far - near));

        mat4x4[0] = x_orth;
        mat4x4[1] = 0;
        mat4x4[2] = 0;
        mat4x4[3] = 0;

        mat4x4[4] = 0;
        mat4x4[5] = y_orth;
        mat4x4[6] = 0;
        mat4x4[7] = 0;

        mat4x4[8] = 0;
        mat4x4[9] = 0;
        mat4x4[10] = z_orth;
        mat4x4[11] = 0;

        mat4x4[12] = tx;
        mat4x4[13] = ty;
        mat4x4[14] = tz;
        mat4x4[15] = 1f;
        return mat4x4;
    }

    public final FloatBuffer get(FloatBuffer floatBuffer16) {
        if (floatBuffer16.capacity() != 16) {
            throw new RuntimeException("Float Buffer Must Have Capacity of 16");
        }
        float x_orth = 2f / (right - left);
        float y_orth = 2f / (top - bottom);
        float z_orth = -2f / (far - near);

        float tx = -((right + left) / (right - left));
        float ty = -((top + bottom) / (top - bottom));
        float tz = -((far + near) / (far - near));

        floatBuffer16.put(x_orth);
        floatBuffer16.put(0f);
        floatBuffer16.put(0f);
        floatBuffer16.put(0f);

        floatBuffer16.put(0f);
        floatBuffer16.put(y_orth);
        floatBuffer16.put(0f);
        floatBuffer16.put(0f);

        floatBuffer16.put(0f);
        floatBuffer16.put(0f);
        floatBuffer16.put(z_orth);
        floatBuffer16.put(0f);

        floatBuffer16.put(tx);
        floatBuffer16.put(ty);
        floatBuffer16.put(tz);
        floatBuffer16.put(0f);

        return floatBuffer16;
    }

    /**
     * Sets the matrix to an orthographic projection like glOrtho
     * (http://www.opengl.org/sdk/docs/man2/xhtml/glOrtho.xml) following the
     * OpenGL equivalent
     *
     * @param matrix
     * @return This matrix for chaining
     */
    public final Matrix4f load(Matrix4f matrix) {
        float x_orth = 2f / (right - left);
        float y_orth = 2f / (top - bottom);
        float z_orth = -2f / (far - near);

        float tx = -((right + left) / (right - left));
        float ty = -((top + bottom) / (top - bottom));
        float tz = -((far + near) / (far - near));

        matrix.mat4x4[0] = x_orth;
        matrix.mat4x4[1] = 0;
        matrix.mat4x4[2] = 0;
        matrix.mat4x4[3] = tx;
        matrix.mat4x4[4] = 0;
        matrix.mat4x4[5] = y_orth;
        matrix.mat4x4[6] = 0;
        matrix.mat4x4[7] = ty;
        matrix.mat4x4[8] = 0;
        matrix.mat4x4[9] = 0;
        matrix.mat4x4[10] = z_orth;
        matrix.mat4x4[11] = tz;
        matrix.mat4x4[12] = 0;
        matrix.mat4x4[13] = 0;
        matrix.mat4x4[14] = 0;
        matrix.mat4x4[15] = 1f;

        return matrix;
    }
}
