/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

import co.cata.math.Matrix4f;

public abstract class Shader implements Lifecycle {

    public String vertexShaderSource;
    public String fragmentShaderSource;
    public int vertexShaderHandle = -1;
    public int fragmentShaderHandle = -1;
    public int programHandle = -1;

    public String getVertexShaderSource() {
        return vertexShaderSource;
    }

    public void setVertexShaderSource(String vertexShaderSource) {
        this.vertexShaderSource = vertexShaderSource;
    }

    public String getFragmentShaderSource() {
        return fragmentShaderSource;
    }

    public void setFragmentShaderSource(String fragmentShaderSource) {
        this.fragmentShaderSource = fragmentShaderSource;
    }

    public int getVertexShaderHandle() {
        return vertexShaderHandle;
    }

    public void setVertexShaderHandle(int vertexShaderHandle) {
        this.vertexShaderHandle = vertexShaderHandle;
    }

    public int getFragmentShaderHandle() {
        return fragmentShaderHandle;
    }

    public void setFragmentShaderHandle(int fragmentShaderHandle) {
        this.fragmentShaderHandle = fragmentShaderHandle;
    }

    public int getProgramHandle() {
        return programHandle;
    }

    public void setProgramHandle(int programHandle) {
        this.programHandle = programHandle;
    }

    public abstract boolean inError();

    public abstract String getVertexCompileError();

    public abstract String getFragmentCompileError();

    public abstract String getLinkError();

    public abstract ShaderAttribute[] getAttributes();

    public abstract boolean hasAttribute(String name);

    public abstract boolean hasAttribute(int location);

    public abstract ShaderAttribute getAttribute(String name);

    public abstract ShaderAttribute getAttribute(int location);

    public abstract int getAttributeLocation(String name);
    
    public abstract Uniform[] getUniforms();

    public abstract boolean hasUniform(String name);

    public abstract boolean hasUniform(int location);

    public abstract Uniform getUniform(String name);

    public abstract Uniform getUniform(int location);
    
    public abstract int getUniformLocation(String name);

    public abstract void enable();
    
    public abstract void disable();
    
    @Override
    public String toString() {
        return "Shader [" + programHandle + " Init:" + isInitialized() + " Loaded:" + isLoaded() + "]";
    }


    public String getState() {
        StringBuilder builder = new StringBuilder();
        builder.append("ShaderProgram:\r\n");
        builder.append("- Vertex Shader Begin\r\n\"");
        builder.append(vertexShaderSource.replace("\n", "\n"));
        builder.append("\"\r\n- Vertex Shader End\r\n");
        builder.append("- Fragment Shader Begin\r\n\"");
        builder.append(fragmentShaderSource.replace("\n", "\n"));
        builder.append("\"\r\n- Fragment Shader End\r\n");

        builder.append("- Vertex Attributes\r\n");

        ShaderAttribute[] attributes = getAttributes();
        if (attributes != null) {
            for (ShaderAttribute prop : attributes) {
                builder.append("   ").append(prop).append("\r\n");
            }
        }

        builder.append("- Uniforms\r\n");

        Uniform[] uniform = getUniforms();
        if (uniform != null) {
            for (Uniform prop : uniform) {
                builder.append("   ").append(prop).append("\r\n");
            }
        }

        return builder.toString();
    }

    public abstract void loadUniform(String name, Matrix4f matrix);
    
    public abstract void loadUniform(int location, Matrix4f matrix);
    
}
