/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

import co.cata.math.Matrix4f;
import co.cata.math.QuickMath;

/**
 *
 * @author jmarsden
 */
public class PerspectiveProjection extends Projection {

    float near;
    float far;
    float fov;
    float height;
    float width;

    /**
     * Default constructor.
     *
     * @param near The near plane
     * @param far The far plane
     * @param fov The field of view in radians
     * @param height The height
     * @param width The width
     */
    public PerspectiveProjection(float near, float far, float fov, float height, float width) {
        this.near = near;
        this.far = far;
        this.fov = QuickMath.toDegrees(fov);
        this.height = height;
	this.width = width;
        load(mat4x4);
    }

    @Override
    public final float[] load(float[] mat4x4) {
        float l_fd = (float) (1.0 / QuickMath.tan((fov * (QuickMath.PI / 180F)) / 2.0F));
        float l_a1 = (far + near) / (near - far);
        float l_a2 = (2 * far * near) / (near - far);
        mat4x4[0] = l_fd / (width / height);
        mat4x4[1] = 0;
        mat4x4[2] = 0;
        mat4x4[3] = 0;
        mat4x4[4] = 0;
        mat4x4[5] = l_fd;
        mat4x4[6] = 0;
        mat4x4[7] = 0;
        mat4x4[8] = 0;
        mat4x4[9] = 0;
        mat4x4[10] = l_a1;
        mat4x4[11] = -1;
        mat4x4[12] = 0;
        mat4x4[13] = 0;
        mat4x4[14] = l_a2;
        mat4x4[15] = 0;
        return mat4x4;
    }

    /**
     * Sets the matrix to a projection matrix with a near- and far plane, a
     * field of view in degrees and an aspect ratio.
     */
    @Override
    public final Matrix4f load(Matrix4f matrix) {
        float l_fd = (float) (1.0 / QuickMath.tan((fov * (QuickMath.PI / 180F)) / 2.0F));
        float l_a1 = (far + near) / (near - far);
        float l_a2 = (2 * far * near) / (near - far);
        matrix.mat4x4[0] = l_fd / (width / height);
        matrix.mat4x4[1] = 0;
        matrix.mat4x4[2] = 0;
        matrix.mat4x4[3] = 0;
        matrix.mat4x4[4] = 0;
        matrix.mat4x4[5] = l_fd;
        matrix.mat4x4[6] = 0;
        matrix.mat4x4[7] = 0;
        matrix.mat4x4[8] = 0;
        matrix.mat4x4[9] = 0;
        matrix.mat4x4[10] = l_a1;
        matrix.mat4x4[11] = -1;
        matrix.mat4x4[12] = 0;
        matrix.mat4x4[13] = 0;
        matrix.mat4x4[14] = l_a2;
        matrix.mat4x4[15] = 0;
        return matrix;
    }
}
