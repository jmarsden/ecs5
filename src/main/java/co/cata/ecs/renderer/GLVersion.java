/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.renderer;

public enum GLVersion {

    GL11(1.1),
    GL12(1.2),
    GL13(1.3),
    GL14(1.4),
    GL15(1.5),
    GL20(2.0),
    GL21(2.1),
    GL30(3.0),
    GL31(3.1),
    GL32(3.2),
    GL33(3.3),
    GL40(4.0),
    GL41(4.1),
    GL42(4.2),
    GL43(4.3),
    GLHMMN(0);
    double version;

    GLVersion(double version) {
        this.version = version;
    }
    
    public double getVersion() {
        return version;
    }
    
    @Override
    public String toString() {
        return "" + version;
    }
}
