/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.runtime;

import co.cata.ecs.renderer.GLVersion;
import co.cata.utils.Color;

public class ApplicationConfiguration {

	public String title;
	
	public int width;
	
	public int height;
	
	public Color backGroundColor;
        
        public boolean resizable;
        
        public boolean fixedViewportScale;
        
        public boolean fullScreen;
        
        public GLVersion forceGLVersion;
        
        public int fpsTarget;
	
	public ApplicationConfiguration() {
		title = "cc.plural.ecs!";
		width = 400;
		height = 300;
		backGroundColor = new Color(25,25,25);
                resizable = false;
                fixedViewportScale = true;
                fullScreen = false;
                forceGLVersion = null;
                fpsTarget = 60;
	}
}
