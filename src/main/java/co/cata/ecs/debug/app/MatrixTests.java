/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.cata.ecs.debug.app;

import co.cata.ecs.renderer.AxisCamera;
import co.cata.ecs.renderer.Camera;
import co.cata.ecs.renderer.PerspectiveProjection;
import co.cata.ecs.renderer.Projection;
import co.cata.math.QuickMath;
import co.cata.utils.MatrixPrinter;
import java.nio.FloatBuffer;
import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

/**
 *
 * @author JOHNMARSDEN
 */
public class MatrixTests {

    FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
    Matrix4f viewMatrix = new Matrix4f();
    Matrix4f projMatrix = new Matrix4f();
    
    public void run() {
	projMatrix.setPerspective((float) Math.toRadians(30), (float) 600 / 600, 0.01f, 50.0f);
	projMatrix.get(matrixBuffer);
	System.out.println(String.format("%s\n%s", "Projection Matrix LWJGL" , MatrixPrinter.glColumnOrderMatrix2String(matrixBuffer)));
	
	float[] array = new float[16];
	Projection projection = new PerspectiveProjection(0.01f, 50.0f, (float) QuickMath.toRadians(30), 600f, 600f);
	projection.load(array);
	System.out.println(String.format("%s\n%s", "Projection Matrix ECS" , MatrixPrinter.glColumnOrderMatrix2String(array)));
	
	viewMatrix.setLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	viewMatrix.get(matrixBuffer);
	System.out.println(String.format("%s\n%s", "View Matrix LWJGL" , MatrixPrinter.glColumnOrderMatrix2String(matrixBuffer)));
	
	Camera camera = new AxisCamera();
	camera.lookAt(0, 0, 5, 0, 0, 0);
	camera.load(array);
	System.out.println(String.format("%s\n%s", "View Matrix ECS" , MatrixPrinter.glColumnOrderMatrix2String(array)));
	
    }
    
    
    public static void main(String[] args) {
	MatrixTests tests = new MatrixTests();
	tests.run();
    }
}
