/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.debug.app;

import co.cata.ecs.provider.LWJGL3Display;
import co.cata.ecs.renderer.Display;
import co.cata.ecs.renderer.Renderer;
import co.cata.ecs.runtime.ApplicationConfiguration;

/**
 *
 * @author johnwm
 */
public class TestDisplay {
    public static void main(String[] args) {
        Display display = new LWJGL3Display();
        display.init(800,600);
        Renderer renderer = display.createRenderer(new ApplicationConfiguration() {
            
        });
        System.out.println(renderer);
    }
}
