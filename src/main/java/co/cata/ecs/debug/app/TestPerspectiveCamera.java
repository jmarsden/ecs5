/**
 * Copyright (C) 2013-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.debug.app;

import co.cata.ecs.engine.Component;
import co.cata.ecs.engine.Engine;
import co.cata.ecs.engine.GameObject;
import co.cata.ecs.provider.LWJGL3Application;
import co.cata.ecs.provider.LWJGL3ApplicationConfiguration;
import co.cata.ecs.renderer.ByteImage;
import co.cata.ecs.renderer.Camera;
import co.cata.ecs.renderer.Display;
import co.cata.ecs.renderer.Platform;
import co.cata.ecs.renderer.Projection;
import co.cata.ecs.renderer.Renderer;
import co.cata.ecs.renderer.Texture;
import co.cata.ecs.runtime.ApplicationInterface;
import co.cata.graphics.Vertex;
import co.cata.math.Matrix3f;
import co.cata.math.Matrix4f;
import co.cata.math.QuickMath;
import co.cata.math.Transformation;
import co.cata.utils.Color;
import co.cata.utils.MatrixPrinter;
import co.cata.utils.PNGDecoder;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author jmarsden
 */
public class TestPerspectiveCamera implements ApplicationInterface {

    public static final String TITLE = "Test Perspective Camera";
    int width;
    int height;
    int fps;
    boolean paused;
    Engine engine;
    Logger logger;

    public static void main(String[] args) throws IOException {

        LWJGL3ApplicationConfiguration applicationConfiguration = new LWJGL3ApplicationConfiguration();
        applicationConfiguration.title = TITLE;
        applicationConfiguration.width = 600;
        applicationConfiguration.height = 480;
        applicationConfiguration.resizable = true;
        applicationConfiguration.backGroundColor = Color.DARK_TURQUOISE;
        LWJGL3Application application = new LWJGL3Application(new TestPerspectiveCamera(), applicationConfiguration);
        application.start();
    }

   private static ByteImage loadIcon(URL url) throws IOException {
        InputStream inputStream = url.openStream();
        try {
            PNGDecoder decoder = new PNGDecoder(inputStream);
            ByteBuffer bytebuf = ByteBuffer.allocateDirect(decoder.getWidth() * decoder.getHeight() * 4);
            decoder.decode(bytebuf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            bytebuf.flip();
            return new ByteImage(decoder.getWidth(), decoder.getHeight(), bytebuf);
        } finally {
            inputStream.close();
        }
    }

    public ByteImage[] loadIcons(Renderer renderer) {
        URL icon16 = Object.class.getResource("/icon/icon16.png");
        URL icon32 = Object.class.getResource("/icon/icon32.png");
        URL icon128 = Object.class.getResource("/icon/icon128.png");
        ByteImage[] icons = null;

        try {
            switch (renderer.getPlatform().platform) {
                case Platform.LINUX:
                    icons = new ByteImage[]{loadIcon(icon32)};
                    break;
                case Platform.OSX:
                    icons = new ByteImage[]{loadIcon(icon128)};
                    break;
                case Platform.WINDOWS:
                    icons = new ByteImage[]{loadIcon(icon16), loadIcon(icon32)};
                    break;
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(TestPerspectiveCamera.class.getName()).log(Level.SEVERE, null, ex);
        }
        return icons;
    }

    public void init(Renderer renderer) {
        renderer.setIcon(loadIcons(renderer));
        renderer.setFrameRate(80);

        logger = LogManager.getLogger(TestECSBasic.class);
        if (logger.isInfoEnabled()) {
            logger.info("init()");
        }
        String rendererDump = renderer.toString();
        if (logger.isInfoEnabled()) {
            logger.info(rendererDump);
        }

	Display display = renderer.getDisplay();
	
	// set perspective projection
        renderer.setPerspectiveProjection(0.01f, 1000.0f, QuickMath.toRadians(60.0f), display.getHeight(), display.getWidth());
	
	Matrix4f projectionMatrix = new Matrix4f();
	Matrix4f viewMatrix = new Matrix4f();
	Matrix4f modelMatrix = new Matrix4f();
	
	Projection projection = renderer.getProjection();
	projection.load(projectionMatrix);
	System.out.println(String.format("%s\n%s", "Projection Matrix" , MatrixPrinter.matrix2String(projectionMatrix)));
	
	// create and set camera
	Camera camera = renderer.getCamera();
	camera.lookAt(0, 0, -5, 0, 0, 0);
	if(!camera.validate()) {
	    System.out.println("Invalid Camera:" + camera);
	}
	camera.load(viewMatrix);
	System.out.println(String.format("%s\n%s", "View Matrix" , MatrixPrinter.matrix2String(viewMatrix)));
	
//	float[] positions = new float[] {
//	    // VO
//	    -0.5f,  0.5f,  0.5f,
//	    // V1
//	    -0.5f, -0.5f,  0.5f,
//	    // V2
//	    0.5f, -0.5f,  0.5f,
//	    // V3
//	     0.5f,  0.5f,  0.5f,
//	    // V4
//	    -0.5f,  0.5f, -0.5f,
//	    // V5
//	     0.5f,  0.5f, -0.5f,
//	    // V6
//	    -0.5f, -0.5f, -0.5f,
//	    // V7
//	     0.5f, -0.5f, -0.5f,
//	};
		
	Vertex v0 = new Vertex(-0.5f,  0.5f,  0.5f, 0.5f, 0.0f, 0.0f);
	Vertex v1 = new Vertex(-0.5f, -0.5f,  0.5f, 0.0f, 0.5f, 0.0f);
	Vertex v2 = new Vertex( 0.5f, -0.5f,  0.5f, 0.0f, 0.0f, 0.5f);
	Vertex v3 = new Vertex( 0.5f,  0.5f,  0.5f, 0.0f, 0.5f, 0.5f);
	Vertex v4 = new Vertex(-0.5f,  0.5f, -0.5f, 0.5f, 0.0f, 0.0f);
	Vertex v5 = new Vertex( 0.5f,  0.5f, -0.5f, 0.0f, 0.5f, 0.0f);
	Vertex v6 = new Vertex(-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 0.5f);
	Vertex v7 = new Vertex( 0.5f, -0.5f, -0.5f, 0.0f, 0.5f, 0.5f);
	
	short[] indices = new short[] {
	    // Front face
	    0, 1, 3, 3, 1, 2,
	    // Top Face
	    4, 0, 3, 5, 4, 3,
	    // Right face
	    3, 2, 7, 5, 3, 7,
	    // Left face
	    6, 1, 0, 6, 0, 4,
	    // Bottom face
	    2, 1, 6, 2, 6, 7,
	    // Back face
	    7, 6, 4, 7, 4, 5,
	};

	engine = new Engine();

	GameObject testObject = engine.createGameObject("Test Game Object");

	testObject.createGeometry();
	//testObject.geometry.texture = texture1;

	testObject.geometry.mesh = renderer.createStaticMesh(new Vertex[]{v0, v1, v2, v3, v4, v5, v6, v7}, indices);
	testObject.geometry.mesh.init();
	testObject.geometry.mesh.load(); 

	testObject.createSpatial();
	testObject.spatial.setTranslation(0f, 0f, 5f);

	Transformation transformation = testObject.getWorld();
        transformation.load(modelMatrix);
	System.out.println(String.format("%s\n%s", "Model Matrix" , MatrixPrinter.matrix2String(modelMatrix)));   
	
        engine.dumpState();
    }

    public void start() {
        engine.start();
    }

    public void update(float delta) {
        if (!paused) {
            engine.update(delta);
        }
    }

    public void render(Renderer renderer) {
        engine.render(renderer);
    }

    public void pause() {
        paused = true;
    }

    public void resume() {
        paused = false;
    }

    public void exit() {
    }

    public void reSizeCallback(int width, int height) {
        this.height = height;
        this.width = width;
    }

    @Override
    public void fpsUpdateCallback(Renderer renderer, int fps) {
        if (this.fps != fps) {
            renderer.getDisplay().setTitle(TITLE + " (" + fps + " fps)");
            this.fps = fps;
        }
    }

    public class RotateGameObject extends Component {

        float rotation;
        float speed;
        Matrix3f matrix3f;

        public RotateGameObject(float rotation, float speed) {
            this.rotation = rotation;
            this.speed = speed;
            this.matrix3f = Matrix3f.identity();
        }

        @Override
        public void init() {
            super.init();
            if (gameObject.spatial == null) {
                gameObject.createSpatial();
            }
            rotation = 0;
        }

        @Override
        public void start() {
            super.start();
        }

        @Override
        public void update(float delta) {
            super.update(delta);
            //System.out.println("Delta:" + delta);
            this.rotation = rotation + (delta * speed);
            gameObject.spatial.setRotation(Transformation.storeZRotationMatrix(matrix3f, QuickMath.PI * rotation));
        }
    }
}
