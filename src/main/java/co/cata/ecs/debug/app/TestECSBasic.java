/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.debug.app;

import co.cata.ecs.engine.Component;
import co.cata.ecs.engine.Engine;
import co.cata.ecs.engine.GameObject;
import co.cata.ecs.provider.LWJGL3Application;
import co.cata.ecs.provider.LWJGL3ApplicationConfiguration;
import co.cata.ecs.renderer.ByteImage;
import co.cata.ecs.renderer.Camera;
import co.cata.ecs.renderer.Platform;
import co.cata.ecs.renderer.Projection;
import co.cata.ecs.renderer.Renderer;
import co.cata.ecs.runtime.ApplicationInterface;
import co.cata.graphics.Vertex;
import co.cata.math.Matrix3f;
import co.cata.math.Matrix4f;
import co.cata.math.QuickMath;
import co.cata.math.Transformation;
import co.cata.math.Vector3f;
import co.cata.utils.Color;
import co.cata.utils.MatrixPrinter;
import co.cata.utils.PNGDecoder;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 *
 * @author jmarsden
 */
public class TestECSBasic implements ApplicationInterface {

    public static final String TITLE = "Test ECS Basic";
    int width;
    int height;
    int fps;
    boolean paused;
    Engine engine;
    Logger logger;

    public static void main(String[] args) throws IOException {
        LWJGL3ApplicationConfiguration applicationConfiguration = new LWJGL3ApplicationConfiguration();
        applicationConfiguration.title = TITLE;
        applicationConfiguration.width = 600;
        applicationConfiguration.height = 480;
        applicationConfiguration.resizable = true;
        applicationConfiguration.backGroundColor = Color.LIGHT_GREY;
        LWJGL3Application application = new LWJGL3Application(new TestECSBasic(), applicationConfiguration);
        application.start();
    }

    private static ByteImage loadIcon(URL url) throws IOException {
        InputStream inputStream = url.openStream();
        try {
            PNGDecoder decoder = new PNGDecoder(inputStream);
            ByteBuffer bytebuf = ByteBuffer.allocateDirect(decoder.getWidth() * decoder.getHeight() * 4);
            decoder.decode(bytebuf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            bytebuf.flip();
            return new ByteImage(decoder.getWidth(), decoder.getHeight(), bytebuf);
        } finally {
            inputStream.close();
        }
    }

    public ByteImage[] loadIcons(Renderer renderer) {
        URL icon16 = Object.class.getResource("/icon/icon16.png");
        URL icon32 = Object.class.getResource("/icon/icon32.png");
        URL icon128 = Object.class.getResource("/icon/icon128.png");
        ByteImage[] icons = null;

        try {
            switch (renderer.getPlatform().platform) {
                case Platform.LINUX:
                    icons = new ByteImage[]{loadIcon(icon32)};
                    break;
                case Platform.OSX:
                    icons = new ByteImage[]{loadIcon(icon128)};
                    break;
                case Platform.WINDOWS:
                    icons = new ByteImage[]{loadIcon(icon16), loadIcon(icon32)};
                    break;
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(TestECSBasic.class.getName()).log(Level.SEVERE, null, ex);
        }
        return icons;
    }
    
    public void init(Renderer renderer) {
        renderer.setIcon(loadIcons(renderer));
        renderer.setFrameRate(80);

        logger = LogManager.getLogger(TestECSBasic.class);
        if (logger.isInfoEnabled()) {
            logger.info("init()");
        }
        String rendererDump = renderer.toString();
        if (logger.isInfoEnabled()) {
            logger.info(rendererDump);
        }

        float left = 0;
        float right = 600;
        float top = 480;
        float bottom = 0;

        renderer.setOrthoProjection(left, right, top, bottom);
	
	Matrix4f projectionMatrix = new Matrix4f();
	Matrix4f viewMatrix = new Matrix4f();
	Matrix4f modelMatrix = new Matrix4f();
	
	Matrix4f initialTransform = new Matrix4f();
	
	Projection projection = renderer.getProjection();
	projection.load(projectionMatrix);
	System.out.println(String.format("%s\n%s", "Projection Matrix" , MatrixPrinter.matrix2String(projectionMatrix)));
	
	Camera camera = renderer.getCamera();
	
	camera.load(viewMatrix);
	System.out.println(String.format("%s\n%s", "View Matrix" , MatrixPrinter.matrix2String(viewMatrix)));

        engine = new Engine();
        
        GameObject testObject = engine.createGameObject("Test Game Object");

        testObject.createGeometry();
        testObject.geometry.texture = renderer.createTexture("/assets/images/texture1.png");
        testObject.geometry.texture.init();
        testObject.geometry.texture.load();

        Vertex v0 = new Vertex();
        v0.setXYZ(0f, 1f, 0);
        v0.setRGB(1, 0, 0);
        v0.setST(0, 0);
        Vertex v1 = new Vertex();
        v1.setXYZ(0f, 0f, 0);
        v1.setRGB(0, 1, 0);
        v1.setST(0, 1);
        Vertex v2 = new Vertex();
        v2.setXYZ(1f, 0f, 0);
        v2.setRGB(0, 0, 1);
        v2.setST(1, 1);
        Vertex v3 = new Vertex();
        v3.setXYZ(1f, 1f, 0);
        v3.setRGB(1, 1, 1);
        v3.setST(1, 0);

        testObject.geometry.mesh = renderer.createStaticMesh(new Vertex[]{v0, v1, v2, v3}, new short[]{0, 1, 2, 2, 3, 0});
        testObject.geometry.mesh.init();
        testObject.geometry.mesh.load();

        testObject.createSpatial();
        testObject.spatial.setTranslation(300, 240);
	testObject.spatial.setScale(100);

	Transformation transformation = testObject.getWorld();
        transformation.load(modelMatrix);
	System.out.println(String.format("%s\n%s", "Model Matrix" , MatrixPrinter.matrix2String(modelMatrix)));   
	
	initialTransform = projectionMatrix.multiply(viewMatrix).multiply(modelMatrix);
	System.out.println(String.format("%s\n%s", "Transform Matrix" , MatrixPrinter.matrix2String(initialTransform)));

	//System.out.println(String.format("%s\n%s", "Vector" , initialTransform.transformVector(new Vector3f(0f, 1f, 0f))));
	
        RotateGameObject rotateGameObject = new RotateGameObject();
        testObject.registerComponent(rotateGameObject);

        engine.dumpState();
    }

    @Override
    public void start() {
        engine.start();
    }

    @Override
    public void update(float delta) {
        if (!paused) {
            engine.update(delta);
        }
    }

    @Override
    public void render(Renderer renderer) {
        engine.render(renderer);
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void exit() {
    }

    @Override
    public void reSizeCallback(int width, int height) {
        this.height = height;
        this.width = width;
    }

    @Override
    public void fpsUpdateCallback(Renderer renderer, int fps) {
        if (this.fps != fps) {
            renderer.getDisplay().setTitle(TITLE + " (" + fps + " fps)");
            this.fps = fps;
        }
    }

    public class RotateGameObject extends Component {

        float rotation;
        
        Matrix3f matrix3f;

        public RotateGameObject() {
            rotation = 0;
            this.matrix3f = Matrix3f.identity();
        }
        
        @Override
        public void init() {
            super.init();
            if (gameObject.spatial == null) {
                gameObject.createSpatial();
            }
            rotation = 0;
        }

        @Override
        public void start() {
            super.start();
        }

        @Override
        public void update(float delta) {
            super.update(delta);
            this.rotation = rotation + (delta * 0.001f);
            gameObject.spatial.setRotation(Transformation.storeZRotationMatrix(matrix3f, QuickMath.PI * rotation));
        }
    }
}
