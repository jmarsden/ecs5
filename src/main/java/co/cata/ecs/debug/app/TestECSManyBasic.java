/**
 * Copyright (C) 2013-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.debug.app;


import static co.cata.ecs.debug.app.TestECSBasic.TITLE;
import co.cata.ecs.engine.Component;
import co.cata.ecs.engine.Engine;
import co.cata.ecs.engine.GameObject;
import co.cata.ecs.provider.LWJGL3Application;
import co.cata.ecs.provider.LWJGL3ApplicationConfiguration;
import co.cata.ecs.renderer.Display;
import co.cata.ecs.renderer.ByteImage;
import co.cata.ecs.renderer.Platform;
import co.cata.ecs.renderer.Renderer;
import co.cata.ecs.renderer.Texture;
import co.cata.ecs.runtime.ApplicationInterface;
import co.cata.graphics.Vertex;
import co.cata.math.Matrix3f;
import co.cata.math.QuickMath;
import co.cata.math.Transformation;
import co.cata.utils.Color;
import co.cata.utils.PNGDecoder;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author jmarsden
 */
public class TestECSManyBasic implements ApplicationInterface {

    public static final String TITLE = "Test ECS Manay Basic";
    int width;
    int height;
    int fps;
    boolean paused;
    Engine engine;
    Logger logger;

    public static void main(String[] args) throws IOException {

        LWJGL3ApplicationConfiguration applicationConfiguration = new LWJGL3ApplicationConfiguration();
        applicationConfiguration.title = TITLE;
        applicationConfiguration.width = 350;
        applicationConfiguration.height = 350;
        applicationConfiguration.resizable = true;
        applicationConfiguration.backGroundColor = Color.SLATE_GRAY;
        LWJGL3Application application = new LWJGL3Application(new TestECSManyBasic(), applicationConfiguration);
        application.start();
    }

   private static ByteImage loadIcon(URL url) throws IOException {
        InputStream inputStream = url.openStream();
        try {
            PNGDecoder decoder = new PNGDecoder(inputStream);
            ByteBuffer bytebuf = ByteBuffer.allocateDirect(decoder.getWidth() * decoder.getHeight() * 4);
            decoder.decode(bytebuf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            bytebuf.flip();
            return new ByteImage(decoder.getWidth(), decoder.getHeight(), bytebuf);
        } finally {
            inputStream.close();
        }
    }

    public ByteImage[] loadIcons(Renderer renderer) {
        URL icon16 = Object.class.getResource("/icon/icon16.png");
        URL icon32 = Object.class.getResource("/icon/icon32.png");
        URL icon128 = Object.class.getResource("/icon/icon128.png");
        ByteImage[] icons = null;

        try {
            switch (renderer.getPlatform().platform) {
                case Platform.LINUX:
                    icons = new ByteImage[]{loadIcon(icon32)};
                    break;
                case Platform.OSX:
                    icons = new ByteImage[]{loadIcon(icon128)};
                    break;
                case Platform.WINDOWS:
                    icons = new ByteImage[]{loadIcon(icon16), loadIcon(icon32)};
                    break;
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(TestECSManyBasic.class.getName()).log(Level.SEVERE, null, ex);
        }
        return icons;
    }

    public void init(Renderer renderer) {
        renderer.setIcon(loadIcons(renderer));
        renderer.setFrameRate(80);

        logger = LogManager.getLogger(TestECSBasic.class);
        if (logger.isInfoEnabled()) {
            logger.info("init()");
        }
        String rendererDump = renderer.toString();
        if (logger.isInfoEnabled()) {
            logger.info(rendererDump);
        }

        float left = 0;
        float right = 10;
        float top = 10;
        float bottom = 0;
        float near = 0;
        float far = 1;

        renderer.setOrthoProjection(left, right, top, bottom, near, far);

        Vertex v0 = new Vertex();
        v0.setXYZ(0f, 1f, 0);
        v0.setST(0, 0);
        Vertex v1 = new Vertex();
        v1.setXYZ(0f, 0f, 0);
        v1.setST(0, 1);
        Vertex v2 = new Vertex();
        v2.setXYZ(1f, 0f, 0);
        v2.setST(1, 1);
        Vertex v3 = new Vertex();
        v3.setXYZ(1f, 1f, 0);
        v3.setST(1, 0);

        Texture texture1 = renderer.createTexture("/assets/images/texture1.png");
        texture1.init();
        texture1.load();

        engine = new Engine();

        for (int i = 0; i < 100; i++) {
            v0.setRGB(QuickMath.random(), QuickMath.random(), QuickMath.random());
            v1.setRGB(QuickMath.random(), QuickMath.random(), QuickMath.random());
            v2.setRGB(QuickMath.random(), QuickMath.random(), QuickMath.random());
            v3.setRGB(QuickMath.random(), QuickMath.random(), QuickMath.random());

            GameObject testObject = engine.createGameObject("Test Game Object");

            testObject.createGeometry();
            testObject.geometry.texture = texture1;

            testObject.geometry.mesh = renderer.createStaticMesh(new Vertex[]{v0, v1, v2, v3}, new short[]{0, 1, 2, 2, 3, 0});
            testObject.geometry.mesh.init();
            testObject.geometry.mesh.load();

            testObject.createSpatial();
            testObject.spatial.setTranslation(QuickMath.random() * 10, QuickMath.random() * 10);

            RotateGameObject rotateGameObject = new RotateGameObject(QuickMath.random(), ((QuickMath.random() > 0.5f) ? 0.001f : -0.001f) * QuickMath.random());
            testObject.registerComponent(rotateGameObject);
        }

        engine.dumpState();
    }

    public void start() {
        engine.start();
    }

    public void update(float delta) {
        if (!paused) {
            engine.update(delta);
        }
    }

    public void render(Renderer renderer) {
        engine.render(renderer);
    }

    public void pause() {
        paused = true;
    }

    public void resume() {
        paused = false;
    }

    public void exit() {
    }

    public void reSizeCallback(int width, int height) {
        this.height = height;
        this.width = width;
    }

    @Override
    public void fpsUpdateCallback(Renderer renderer, int fps) {
        if (this.fps != fps) {
            renderer.getDisplay().setTitle(TITLE + " (" + fps + " fps)");
            this.fps = fps;
        }
    }

    public class RotateGameObject extends Component {

        float rotation;
        float speed;
        Matrix3f matrix3f;

        public RotateGameObject(float rotation, float speed) {
            this.rotation = rotation;
            this.speed = speed;
            this.matrix3f = Matrix3f.identity();
        }

        @Override
        public void init() {
            super.init();
            if (gameObject.spatial == null) {
                gameObject.createSpatial();
            }
            rotation = 0;
        }

        @Override
        public void start() {
            super.start();
        }

        @Override
        public void update(float delta) {
            super.update(delta);
            //System.out.println("Delta:" + delta);
            this.rotation = rotation + (delta * speed);
            gameObject.spatial.setRotation(Transformation.storeZRotationMatrix(matrix3f, QuickMath.PI * rotation));
        }
    }
}
