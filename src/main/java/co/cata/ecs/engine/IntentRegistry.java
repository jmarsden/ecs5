/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ecs.engine;

import java.util.ArrayList;
import java.util.List;

public class IntentRegistry {

	final List<Class<? extends ComponentIntent>> registry;
	
	private static IntentRegistry singleton;
	
	static {
		singleton = null;
	}
	
	public IntentRegistry() {
		registry = new ArrayList<Class<? extends ComponentIntent>>();
	}
	
	public int registerIntent(Class<? extends ComponentIntent> intentClass) {
		synchronized(registry) {
			if(registry.contains(intentClass)) {
				return registry.indexOf(intentClass);
			}
			registry.add(intentClass);
			// TODO Can I just use size()-1?
			return registry.indexOf(intentClass);
		}
	}
	
	public int registerIntent(ComponentIntent intent) {
		Class<? extends ComponentIntent> intentClass = intent.getClass();
		return registerIntent(intentClass);
	}
	
	public int lookupIntent(Class<? extends ComponentIntent> intentClass) {
        if(registry.contains(intentClass)) {
            return registry.indexOf(intentClass);
        }
		return registerIntent(intentClass);
	}
	
	public static IntentRegistry lookupSingleton() {
		if(singleton == null) {
			singleton = new IntentRegistry();
		}
		return singleton;
	}
}
