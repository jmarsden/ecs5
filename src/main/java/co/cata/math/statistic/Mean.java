/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math.statistic;

import java.lang.reflect.Array;

/**
 * Mean utility.
 *
 * @author j.w.marsden@gmail.com
 */
public class Mean {

    /**
     * Sum of added values
     */
    double sum;
    /**
     * Count of added values
     */
    int count;

    /**
     * Default Constructor
     */
    public Mean() {
        sum = 0D;
        count = 0;
    }

    /**
     * Adds a moment (new value to be calculated in the mean).
     * @param value new moment value.
     */
    public void addMoment(double value) {
        sum += value;
        count++;
    }

    /**
     * Finds the count of the moments added to this Mean.
     * @return int the count of moments added.
     */
    public int getCount() {
        return count;
    }

    /**
     * Evaluates the value for the current state of the Mean.
     * @return The mean value.
     */
    public double evaulate() {
        if (count == 0) {
            return 0D;
        }
        return sum / (double) count;
    }

    /**
     * Rest the mean.
     */
    public void reset() {
        sum = 0D;
        count = 0;
    }

    /**
     * Calculates the mean of the provided vector of data. 
     * @param data data to calculate the mean.
     * @return mean
     */
    public static Double evaluate(Double[] data) {
        if (data == null) {
            throw new NullPointerException();
        }
        int length = Array.getLength(data);
        if (length == 0) {
            return 0D;
        }
        double sum = 0;
        for (int i = 0; i < length; i++) {
            sum += data[i];
        }
        return sum / length;
    }
}
