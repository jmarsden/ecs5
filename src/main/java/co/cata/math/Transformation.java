/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math;

public class Transformation {

    public Vector3f translation;
    public Vector3f scale;
    public Matrix3f rotation;

    public Transformation() {
        translation = new Vector3f();
        scale = new Vector3f(1f, 1f, 1f);
        rotation = Matrix3f.identity();
        
    }

    public float[] load(float[] mat4x4) {
        mat4x4[0] = scale.x * rotation.m00;
        mat4x4[1] = scale.y * rotation.m01;
        mat4x4[2] = scale.z * rotation.m02;
        mat4x4[3] = translation.x;

        mat4x4[4] = scale.x * rotation.m10;
        mat4x4[5] = scale.y * rotation.m11;
        mat4x4[6] = scale.z * rotation.m12;
        mat4x4[7] = translation.y;

        mat4x4[8] = scale.x * rotation.m20;
        mat4x4[9] = scale.y * rotation.m21;
        mat4x4[10] = scale.z * rotation.m22;
        mat4x4[11] = translation.z;

        mat4x4[12] = 0f;
        mat4x4[13] = 0f;
        mat4x4[14] = 0f;
        mat4x4[15] = 1f;

        return mat4x4;
    }

    public Matrix4f load(Matrix4f transformation) {
        transformation.mat4x4[0] = scale.x * rotation.m00;
        transformation.mat4x4[1] = scale.y * rotation.m01;
        transformation.mat4x4[2] = scale.z * rotation.m02;
        transformation.mat4x4[3] = translation.x;

        transformation.mat4x4[4] = scale.x * rotation.m10;
        transformation.mat4x4[5] = scale.y * rotation.m11;
        transformation.mat4x4[6] = scale.z * rotation.m12;
        transformation.mat4x4[7] = translation.y;

        transformation.mat4x4[8] = scale.x * rotation.m20;
        transformation.mat4x4[9] = scale.y * rotation.m21;
        transformation.mat4x4[10] = scale.z * rotation.m22;
        transformation.mat4x4[11] = translation.z;

        transformation.mat4x4[12] = 0f;
        transformation.mat4x4[13] = 0f;
        transformation.mat4x4[14] = 0f;
        transformation.mat4x4[15] = 1f;

        return transformation;
    }

    public void setIdentity() {
        rotation.setIdentity();
        scale.setData(1, 1, 1);
        translation.setData(0, 0, 0);
    }

    public void store(Transformation t) {
        rotation.setData(t.rotation);
        scale.setData(t.scale);
        translation.setData(t.translation);
    }

    public void concatinate(Transformation parent) {
        scale.multiply(parent.scale);
        rotation.multiply(parent.rotation);
        translation.multiply(parent.scale);
        parent.rotation.transformVector3f(translation).add(parent.translation);
    }

    /**
     * Create a Z Axis Rotation Matrix3f.
     * 
     * @param angle The angle to rotate to (degrees I think)
     * @param matrix3f The output matrix as input
     * @return The Matrix
     */
    public static Matrix3f storeZRotationMatrix(Matrix3f matrix3f, float angle) {
        matrix3f.setData(0, 0, QuickMath.cos(angle));
        matrix3f.setData(1, 0, -QuickMath.sin(angle));
        matrix3f.setData(0, 1, QuickMath.sin(angle));
        matrix3f.setData(1, 1, QuickMath.cos(angle));
        return matrix3f;

    }
}
