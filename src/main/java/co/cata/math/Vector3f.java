/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package co.cata.math;

import java.lang.reflect.Array;

/**
 * Performance optimised 3 dimensional float vector.
 *
 * @author j.w.marsden@gmail.com
 */
public final class Vector3f extends VectorF {

    /**
     * Instance variables for x, y and z components.
     */
    public float x, y, z;
    public static final Vector3f ZERO, X_UNIT, Y_UNIT, Z_UNIT;

    static {
	ZERO = new Vector3f(0F, 0F, 0F);
	X_UNIT = new Vector3f(1F, 0F, 0F);
	Y_UNIT = new Vector3f(0F, 1F, 0F);
	Z_UNIT = new Vector3f(0F, 0F, 1F);
    }

    /**
     * Default constructor (defaults to mutable 0,0,0).
     */
    public Vector3f() {
	super(3);
	this.x = 0;
	this.y = 0;
	this.z = 0;
    }

    /**
     * Default constructor (defaults to not mutable).
     *
     * @param x component for x
     * @param y component for y
     * @param z component for z
     */
    public Vector3f(float x, float y, float z) {
	super(3);
	this.x = x;
	this.y = y;
	this.z = z;
    }

    public Vector3f(Number[] data) {
	super(3);
	if (Array.getLength(data) != 3) {
	    throw new IllegalArgumentException("Number[] data does not have cardinality of three");
	}
	x = data[X].floatValue();
	y = data[Y].floatValue();
	z = data[Z].floatValue();
    }

    public final float getX() {
	return x;
    }

    public final void setX(float x) {
	this.x = x;
    }

    public final float getY() {
	return y;
    }

    public final void setY(float y) {
	this.y = y;
    }

    public final float getZ() {
	return z;
    }

    public final void setZ(float z) {
	this.z = z;
    }

    @Override
    public final Float[] getData() {
	return new Float[]{x, y, z};
    }

    @Override
    public final Float getData(int i) {
	if (i == X) {
	    return x;
	} else if (i == Y) {
	    return y;
	} else if (i == Z) {
	    return z;
	} else {
	    throw new IllegalArgumentException("index i must be 0 <= i < 3");
	}
    }

    @Override
    public final void setData(Number[] data) {
	if (Array.getLength(data) != 3) {
	    throw new IllegalArgumentException("Number[] data does not have cardinality of three");
	}
	x = data[X].floatValue();
	y = data[Y].floatValue();
	z = data[Z].floatValue();
    }

    @Override
    public final void setData(int i, Number data) {
	if (i == X) {
	    x = data.floatValue();
	} else if (i == Y) {
	    y = data.floatValue();
	} else if (i == Z) {
	    z = data.floatValue();
	} else {
	    throw new IllegalArgumentException("index i must be 0 <= i < 3");
	}
    }

    public void setData(Vector3f data) {
	x = data.x;
	y = data.y;
	z = data.z;
    }

    public void setData(float x, float y, float z) {
	this.x = x;
	this.y = y;
	this.z = z;
    }

    public Vector3f add(Vector3f b) {
	x += b.x;
	y += b.y;
	z += b.z;
	return this;
    }

    public Vector3f subtract(Vector3f b) {
	x -= b.x;
	y -= b.y;
	z -= b.z;
	return this;
    }
    
    public Vector3f subtract(Vector3f b, Vector3f c) {
	c.x = x - b.x;
	c.y = y - b.y;
	c.z = z - b.z;
	return c;
    }

    public float dot(Vector3f b) {
	return x * b.x + y * b.y + z * b.z;
    }

    public Vector3f cross(Vector3f b) {
	return cross(b, new Vector3f());
    }

    public Vector3f cross(Vector3f b, Vector3f r) {
	r.x = y * b.z - z * b.y;
	r.y = z * b.x - x * b.z;
	r.z = x * b.y - y * b.x;

	return r;
    }

    public Vector3f addScalar(float v) {
	x += v;
	y += v;
	z += v;
	return this;
    }

    public Vector3f subtractScalar(float v) {
	x -= v;
	y -= v;
	z -= v;
	return this;
    }

    public Vector3f multiplyScalar(float v) {
	x *= v;
	y *= v;
	z *= v;
	return this;
    }

    public Vector3f multiplyScalar(float v, Vector3f r) {
	r.x = x * v;
	r.y = y * v;
	r.z = z * v;
	return r;
    }

    public Vector3f divideScalar(float v) {
	x /= v;
	y /= v;
	z /= v;
	return this;
    }

    public Vector3f multiply(Vector3f v) {
	x *= v.x;
	y *= v.y;
	z *= v.z;
	return this;
    }

    public Vector3f multiply(Vector3f v, Vector3f r) {
	r.x = x * v.x;
	r.y = y * v.y;
	r.z = z * v.z;
	return r;
    }

    public Vector3f transform(Matrix4f transform) {
	float tX = x * transform.mat4x4[0] + y * transform.mat4x4[5] + z * transform.mat4x4[9] + 1.0f * transform.mat4x4[13];
	float tY = x * transform.mat4x4[1] + y * transform.mat4x4[6] + z * transform.mat4x4[10] + 1.0f * transform.mat4x4[14];
	float tZ = x * transform.mat4x4[2] + y * transform.mat4x4[7] + z * transform.mat4x4[11] + 1.0f * transform.mat4x4[15];
	x = tX;
	y = tY;
	z = tZ;
	return this;
    }

    @Override
    public VectorF negative() {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector negative(Vector r) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Float magnitude() {
	return QuickMath.sqrt(QuickMath.pow(x, 2) + QuickMath.pow(y, 2) + QuickMath.pow(z, 2));
    }

    @Override
    public Vector3f normalise() {
	float m = magnitude();
	if (m == 0) {
	    x = 0;
	    y = 0;
	    z = 0;
	    return this;
	}
	x = x / m;
	y = y / m;
	z = z / m;
	return this;
    }

    @Override
    public Vector normalise(Vector r) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VectorF add(Vector b) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector add(Vector b, Vector c) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VectorF subtract(Vector b) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector subtract(Vector b, Vector c) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VectorF cross(Vector b) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector cross(Vector b, Vector c) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VectorF addScalar(Number v) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector addScalar(Number v, Vector r) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VectorF subtractScalar(Number v) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector subtractScalar(Number v, Vector r) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VectorF multiplyScalar(Number v) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector multiplyScalar(Number v, Vector r) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VectorF divideScalar(Number v) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vector divideScalar(Number v, Vector r) {
	throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object b) {
	if (b instanceof Vector3f) {
	    Vector3f bVec = (Vector3f) b;
	    return x == bVec.x && y == bVec.y && z == bVec.z;
	} else {
	    return super.equals(b);
	}
    }
}
