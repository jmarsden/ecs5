/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math;

/**
 * Generic float Vector Instance. Provides all the functionality shared float
 * based Vector classes.
 *
 * @author j.w.marsden@gmail.com
 */
public abstract class VectorF extends Vector {

    public VectorF(int n) {
        this.n = n;
    }

    public VectorF(Number[] data) {
        n = data.length;
    }

    @Override
    public abstract Float[] getData();

    @Override
    public abstract Float getData(int i);

    @Override
    public abstract void setData(Number[] data);

    @Override
    public abstract void setData(int i, Number data);

    @Override
    public Float magnitude() {
        VectorF a = this;
        double squaredSum = 0;
        for (int i = 0; i < n; i++) {
            squaredSum += QuickMath.pow(a.getData(i), 2);
        }
        return (float) QuickMath.sqrt(squaredSum);
    }
    
    @Override
    public Float magnitudeSquared() {
        VectorF a = this;
        double squaredSum = 0;
        for (int i = 0; i < n; i++) {
            squaredSum += Math.pow(a.getData(i), 2);
        }
        return (float) squaredSum;
    }

    @Override
    public final Float length() {
        return magnitude();
    }

    @Override
    public abstract VectorF negative();
    
    @Override
    public abstract Vector negative(Vector r);

    @Override
    public abstract VectorF normalise();
    
    @Override
    public abstract Vector normalise(Vector r);

    @Override
    public abstract VectorF add(Vector b);
    
    @Override
    public abstract Vector add(Vector b, Vector c);

    @Override
    public abstract VectorF subtract(Vector b);

    @Override
    public abstract Vector subtract(Vector b, Vector c);

    @Override
    public Number dot(Vector b) {
        float dotProduct = 0;
        return dot(b, dotProduct);
    }
    
    @Override
    public Number dot(Vector b, Number r) {
        if (n != b.n) {
            throw new RuntimeException("Vector dimensions are not equal.");
        }
        float dotProduct = 0;
        for (int i = 0; i < n; i++) {
            dotProduct += getData(i) * b.getData(i).doubleValue();
        }
        r = dotProduct;
        return r;
    }

    @Override
    public abstract VectorF cross(Vector b);
    
    @Override
    public abstract Vector cross(Vector b, Vector c);

    @Override
    public abstract VectorF addScalar(Number v);
    
    @Override
    public abstract Vector addScalar(Number v, Vector r);

    @Override
    public abstract VectorF subtractScalar(Number v);
    
    @Override
    public abstract Vector subtractScalar(Number v, Vector r);

    @Override
    public abstract VectorF multiplyScalar(Number v);
    
    @Override
    public abstract Vector multiplyScalar(Number v, Vector r);

    @Override
    public abstract VectorF divideScalar(Number v);
    
    @Override
    public abstract Vector divideScalar(Number v, Vector r);

    @Override
    public String toString() {
        String dataString = "{";
        for (int i = 0; i < n; i++) {
            dataString += getData(i) + ((i < n - 1) ? ", " : "");
        }
        dataString += "}";
        return String.format("%s %s", super.toString(), dataString);
    }

    public static VectorF empty(int i) {
        VectorF e = null;
        switch(i) {
            case 2:
                e = new Vector2f();
            case 3:
                e = new Vector3f();
            default:
            	//todo: port ArrayVectorF
                //e = new ArrayVectorF(i);
        }
        for(int j=0;j<i;j++) {
            e.setData(i, 0D);
        }
        return e;
    }

    public static VectorF random(int i) {
        VectorF r = empty(i);
        for(int j=0;j<i;j++) {
            r.setData(i, QuickMath.random());
        }
        return null;
    }
}
