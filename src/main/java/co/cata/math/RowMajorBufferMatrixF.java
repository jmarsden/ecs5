/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math;

import java.nio.FloatBuffer;

public class RowMajorBufferMatrixF extends MatrixF {

	FloatBuffer bufferData;
	
	public RowMajorBufferMatrixF(Float[][] data) {
        m = data.length;
        if (m > 0) {
            n = data[0].length;
        } else {
            throw new IllegalArgumentException("data dimensions must be > 0");
        }
        if (m != n) {
            throw new IllegalArgumentException("data dimensions must be equal");
        }
        bufferData = FloatBuffer.allocate(m*n);
        
        for(int i=0;i<m;i++) {
        	for(int j=0;j<n;j++) {
        		bufferData.put(data[i][j]);
        	}
        }
	}
	
	public RowMajorBufferMatrixF(float[][] data) {
        m = data.length;
        if (m > 0) {
            n = data[0].length;
        } else {
            throw new IllegalArgumentException("data dimensions must be > 0");
        }
        if (m != n) {
            throw new IllegalArgumentException("data dimensions must be equal");
        }
        bufferData = FloatBuffer.allocate(m*n);
        for(int i=0;i<m;i++) {
        	bufferData.put(data[i]);
        }
	}
	
	public RowMajorBufferMatrixF(float[] data) {
		int s = data.length;
		if(!QuickMath.isPerfectSquare(s)) {
			throw new IllegalArgumentException("data dimensions must result in a perfect square");
		}
		for(int i=0;i<s;i++) {
			bufferData.put(data[i]);
		}
	}

	@Override
	public Float[][] getData() {
		bufferData.reset();
		Float[][] result = new Float[m][n];
        for(int i=0;i<m;i++) {
        	for(int j=0;j<n;j++) {
        		result[m][n] = bufferData.get();
        	}
        }
		return result;
	}

	@Override
	public Float getData(int i, int j) {
		bufferData.reset();
		return bufferData.get(i*n+j);
	}

	@Override
	public void setData(Number[][] data) {
		m = data.length;
        if (m > 0) {
            n = data[0].length;
        } else {
            throw new IllegalArgumentException("data dimensions must be > 0");
        }
        if (m != n) {
            throw new IllegalArgumentException("data dimensions must be equal");
        }
        bufferData = FloatBuffer.allocate(m*n);
        for(int i=0;i<m;i++) {
        	for(int j=0;j<n;j++) {
        		Number number = data[i][j];
        		if(number != null) {
        			bufferData.put(data[i][j].floatValue());
        		} else {
        			bufferData.put(0);
        		}
        	}
        }
	}

	@Override
	public void setData(int i, int j, Number data) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public Float[] getRow(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF add(Matrix b) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF add(Matrix b, Matrix c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF subtract(Matrix b) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF subtract(Matrix b, Matrix c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF multiply(Matrix b) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF multiply(Matrix b, Matrix c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF transpose() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF addScalar(Number v) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF addScalar(Number v, Matrix mutate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF subtractScalar(Number v) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF subtractScalar(Number v, Matrix c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF multiplyScalar(Number v) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF multiplyScalar(Number v, Matrix c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF divideScalar(Number v) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF divideScalar(Number v, Matrix c) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VectorF transformVector(VectorF v) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MatrixF invert() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Matrix transpose(Matrix r) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Matrix invert(Matrix r) {
		// TODO Auto-generated method stub
		return null;
	}
}
