/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math;

import java.lang.reflect.Array;

/**
 * Performance optimised 2 dimensional float vector.
 *
 * @author j.w.marsden@gmail.com
 */
public class Vector2f extends VectorF {

    /**
     * Instance variables for x and y components.
     */
    public float x, y;
    public static final Vector2f ZERO, X_UNIT, Y_UNIT;

    static {
        ZERO = new Vector2f(0F, 0F);
        X_UNIT = new Vector2f(1F, 0F);
        Y_UNIT = new Vector2f(0F, 1F);
    }

    /**
     * Default constructor (defaults to mutable 0,0).
     */
    public Vector2f() {
        super(2);
        this.x = 0;
        this.y = 0;
    }

    /**
     * Default x,y constructor (defaults to not mutable).
     * @param x component for x
     * @param y component for y
     */
    public Vector2f(float x, float y) {
        super(2);
        this.x = x;
        this.y = y;
    }


    public Vector2f(Number[] data) {
        super(2);
        if (Array.getLength(data) >= 2) {
            throw new IllegalArgumentException("Number[] data does not have cardinality of three");
        }
        x = data[X].floatValue();
        y = data[Y].floatValue();
    }

    public final float getX() {
        return x;
    }

    public final void setX(float x) {
        this.x = x;
    }

    public final float getY() {
        return y;
    }

    public final void setY(float y) {
        this.y = y;
    }

    @Override
    public final Float[] getData() {
        return new Float[]{x, y};
    }

    @Override
    public final Float getData(int i) {
        if (i == X) {
            return x;
        } else if (i == Y) {
            return y;
        } else {
            throw new IllegalArgumentException("index i must be 0 <= i < 2");
        }
    }

    @Override
    public final void setData(Number[] data) {
        if (Array.getLength(data) != 2) {
            throw new IllegalArgumentException("Number[] data does not have cardinality of three");
        }
        x = data[X].floatValue();
        y = data[Y].floatValue();
    }

    @Override
    public final void setData(int i, Number data) {
        if (i == X) {
            x = data.floatValue();
        } else if (i == Y) {
            y = data.floatValue();
        } else {
            throw new IllegalArgumentException("index i must be 0 <= i < 2");
        }
    }
    
    public void setData(Vector2f data) {
        x = data.x;
        y = data.y;
    }

	@Override
	public Vector2f negative() {
        x *= -1;
        y *= -1;
        return this;
	}

	@Override
	public Vector negative(Vector r) {
		r.setData(X, x * -1); 
		r.setData(Y, y * -1); 
		return r;
	}
	
	public Vector2f negative2f(Vector2f r) {
		r.x = x * -1;
        r.y = y * -1;
		return r;
	}

	@Override
	public Vector2f normalise() {
        float m = magnitude();
        if (m == 0) {
        	x = 0;
        	y = 0;
            return this;
        }
        x = x / m;
        y = y / m;
        return this;
	}

	@Override
	public Vector normalise(Vector r) {
		throw new UnsupportedOperationException("Not supported yet.");
	}


	@Override
	public Vector2f add(Vector b) {
		if (n != b.n) {
			throw new RuntimeException("Vector dimensions are not equal.");
		}
		x += b.getData(X).floatValue();
		y += b.getData(Y).floatValue();
		return this;
	}

	@Override
	public Vector add(Vector b, Vector c) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

    public final Vector2f add2f(Vector2f b) {
        x += b.x;
        y += b.y;
        return this;
    }
	
	@Override
	public Vector2f subtract(Vector b) {
	    if (n != b.n) {
            throw new RuntimeException("Vector dimensions are not equal.");
        }
        x -= b.getData(X).floatValue();
        y -= b.getData(Y).floatValue();
        return this;
	}

	@Override
	public Vector subtract(Vector b, Vector c) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

    public final Vector2f subtract2f(Vector2f b) {
        x -= b.x;
        y -= b.y;
        return this;
    }
	
	@Override
	public Vector2f cross(Vector b) {
		throw new UnsupportedOperationException("Cross does not apply to a two dimensional vector.");
	}

	@Override
	public Vector cross(Vector b, Vector c) {
		throw new UnsupportedOperationException("Cross does not apply to a two dimensional vector.");
	}

	@Override
	public Vector2f addScalar(Number v) {
        x += v.floatValue();
        y += v.floatValue();
        return this;
	}

	@Override
	public Vector addScalar(Number v, Vector r) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
	
    public final Vector2f addScalar2F(float v) {
        x += v;
        y += v;
        return this;
    }

	@Override
	public Vector2f subtractScalar(Number v) {
        x -= v.floatValue();
        y -= v.floatValue();
        return this;
	}

	@Override
	public Vector subtractScalar(Number v, Vector r) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
	
    public final Vector2f subtractScalar2f(float v) {
        x -= v;
        y -= v;
        return this;
    }

	@Override
	public Vector2f multiplyScalar(Number v) {
        x *= v.floatValue();
        y *= v.floatValue();
        return this;
	}

	@Override
	public Vector multiplyScalar(Number v, Vector r) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
	
    public final Vector2f multiplyScalar2f(float v) {
        x *= v;
        y *= v;
        return this;
    }

	@Override
	public Vector2f divideScalar(Number v) {
        if (v.intValue() == 0) {
            throw new RuntimeException("Divide By Zero.");
        }
        x /= v.floatValue();
        y /= v.floatValue();
        return this;
	}

	@Override
	public Vector divideScalar(Number v, Vector r) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

    public final Vector2f divideScalar2f(float v) {
        if (v == 0F) {
            throw new RuntimeException("Divide By Zero.");
        }
        x /= v;
        y /= v;
        return this;
    }
	
	public Vector2f transform(Matrix3f transform) {
        float tX = x*transform.m00 + y*transform.m10;
        float tY = x*transform.m01 + y*transform.m11;
        x = tX;
        y = tY;
        return this;
    }
    
    public static Vector2f zero() {
        return ZERO;
    }
}
