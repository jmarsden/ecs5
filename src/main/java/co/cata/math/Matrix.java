/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.math;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Abstract m by n Matrix in the following form.
 * <pre>
 *         n1  n2  n3 ...  nN
 *      +---------------------
 *   m1 | a11 a12 a13 ... a1n
 *   m2 | a21 a22 a23 ... a2n
 *   m3 | a31 a32 a33 ... a3n
 *   .. | ... ... ...     ...
 *   mN | am1 am2 am3 ... amn
 * </pre>
 * 
 * Provides static methods to construct various Matrix instances.
 *
 * @author j.w.marsden@gmail.com
 */
public abstract class Matrix {

    /**
     * m dimension
     */
    int m;
    /**
     * n dimension
     */
    int n;

    /**
     * matrix data
     */
    /**
     * Find the number of rows in the Matrix (M)
     * @return row count.
     */
    public final int getM() {
        return m;
    }

    /**
     * Find the number of columns in the Matrix (N)
     * @return column count.
     */
    public final int getN() {
        return n;
    }

    /**
     * Retrieve the data from the Matrix. This will be unsupported on some
     * implementations.
     * @return Number[][] data from this Matrix.
     */
    public abstract Number[][] getData();

    /**
     * Retrieve data from the Matrix. This should work on all implementations.
     * @param i The row to extract the data from.
     * @param j The column to extract the data from.
     * @return The data
     */
    public abstract Number getData(int i, int j);

    /**
     * Set Data into the Matrix. This method will be unavailable on sparse
     * implementations.
     * @param data The data to set
     */
    public abstract void setData(Number[][] data);

    /**
     * Set Data into the Matrix. This should work on all implementations.
     * @param i The row to set the data to.
     * @param j The column to set the data to.
     * @param data The data to set.
     */
    public abstract void setData(int i, int j, Number data);

    /**
     * Matrix equality check. True when A = B, (a[i,j]) = (b[i,j]) where
     * 0 <= i < m and 0 <= j < n.
     * @param b Matrix B.
     * @return boolean true when equal otherwise false.
     */
    public boolean equals(Matrix b) {
        Number[][] bData = b.getData();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!getData(i, j).equals(bData[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Extract a row from the Matrix
     * @param i The row number to extract where i < m
     * @return The Row
     */
    public abstract Number[] getRow(int i);

    /**
     * Extract a column from the Matrix
     * @param i The column number to extract where i < n
     * @return The Column
     */
    public abstract Number[] getColumn(int i);

    /**
     * Adds a Matrix to this instance. Matrix A + Matrix B = Matrix C.
     * @param b Matrix B.
     * @return Matrix Matrix C.
     */
    public abstract Matrix add(Matrix b);

    /**
     * Adds a Matrix to this instance. Matrix A + Matrix B = Matrix C.
     * @param b Matrix B.
     * 
     * @return Matrix Matrix C.
     */
    public abstract Matrix add(Matrix b, Matrix c);

    /**
     * Subtracts a Matrix from this instance. Matrix A - Matrix B = Matrix C.
     * @param b Matrix B.
     * @return Matrix Matrix C.
     */
    public abstract Matrix subtract(Matrix b);

    /**
     * Subtracts a Matrix from this instance. Matrix A - Matrix B = Matrix C.
     * @param b Matrix B.
     * @param 
     * @return Matrix Matrix C.
     */
    public abstract Matrix subtract(Matrix b, Matrix c);

    /**
     * Multiplies a Matrix to this instance. Matrix A * Matrix B = Matrix C.
     * @param b Matrix B.
     * @return Matrix Matrix C.
     */
    public abstract Matrix multiply(Matrix b);

    /**
     * Multiplies a Matrix to this instance. Matrix A * Matrix B = Matrix C.
     * @param b Matrix B.
     * @param 
     * @return Matrix Matrix C.
     */
    public abstract Matrix multiply(Matrix b, Matrix c);

    /**
     * Returns the transpose of this instance. Matrix A' = transpose(Matrix A)
     * @return Matrix A' which is a transpose of this instance.
     */
    public abstract Matrix transpose();

    public abstract Matrix transpose(Matrix r);
    
    /**
     * Adds a scalar value to this Matrix instance. Matrix C = C(c[i,j]) = a[i,j]+v
     * @param v Value to add
     * @return Matrix Matrix C
     */
    public abstract Matrix addScalar(Number v);

    /**
     * Adds a scalar value to this Matrix instance. Matrix C = C(c[i,j]) = a[i,j]+v
     * @param v Value to add
     * @param 
     * @return Matrix Matrix C
     */
    public abstract Matrix addScalar(Number v, Matrix c);

    /**
     * Adds a scalar value to this Matrix instance. Matrix C = C(c[i,j]) = a[i,j]-v
     * @param v Value to subtract
     * @return Matrix Matrix C
     */
    public abstract Matrix subtractScalar(Number v);

    /**
     * Adds a scalar value to this Matrix instance. Matrix C = C(c[i,j]) = a[i,j]-v
     * @param v Value to subtract
     * @param 
     * @return Matrix Matrix C
     */
    public abstract Matrix subtractScalar(Number v, Matrix c);

    /**
     * Adds a scalar value to this Matrix instance. Matrix C = C(c[i,j]) = a[i,j]*v
     * @param v Value to multiply
     * @return Matrix Matrix C
     */
    public abstract Matrix multiplyScalar(Number v);

    /**
     * Adds a scalar value to this Matrix instance. Matrix C = C(c[i,j]) = a[i,j]*v
     * @param v Value to multiply
     * @param mutate flag to specify mutation.
     * @return Matrix Matrix C
     */
    public abstract Matrix multiplyScalar(Number v, Matrix c);

    /**
     * Adds a scalar value to this Matrix instance. Matrix C = C(c[i,j]) = a[i,j]/v
     * @param v Value to divide
     * @return Matrix Matrix C
     */
    public abstract Matrix divideScalar(Number v);

    /**
     * Adds a scalar value to this Matrix instance. Matrix C = C(c[i,j]) = a[i,j]/v
     * @param v Value to divide
     * @param mutate flag to specify mutation.
     * @return Matrix Matrix C
     */
    public abstract Matrix divideScalar(Number v, Matrix c);

    /**
     * Extracts a sub-matrix sA from A.
     * @param mi The index of the starting row.
     * @param mj The index of the finishing row.
     * @param ni The index of the starting column.
     * @param nj The index of the finishing column.
     * @return new m by n Matrix where m = mj-mi and n = nj - ni
     */
    public abstract Matrix getSubMatrix(int mi, int mj, int ni, int nj);
    
    public abstract Matrix getSubMatrix(int mi, int mj, int ni, int nj, Matrix r);

    /**
     * Return the invert of the Matrix A.
     * @return Matrix A^-1
     */
    public abstract Matrix invert();
    
    public abstract Matrix invert(Matrix r);

    /**
     * Matrix Solver in the form A*X=B where A is this Matrix and X is the
     * solutions.
     * @param b Matrix B
     * @return Solution Matrix X
     */
    public abstract Matrix solve(Matrix b);
    
    public abstract Matrix solve(Matrix b, Matrix c);

    /**
     * Find the determinant of this Matrix. Must be a square Matrix (n=m)
     * @return The determinant of the Matrix.
     */
    public abstract Number determinant();
    
    public abstract Number determinant(Number r);
    
    /* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Matrix)) {
			return false;
		}
		Matrix o = (Matrix) obj;
		
		for(int i=0;i<m;i++) {
			for(int j=0;j<n;j++) {
				if(!getData(i,j).equals(o.getData(i,j))) {
					return false;
				}
			}
		}
		
		return true;
	}

	/**
     * Prints the Matrix to the System.out (Stdout)
     * @param w Width for each cell.
     * @param d Number of decimal points for each cell.
     */
    public void print(int w, int d) {
        print(new PrintWriter(System.out, true), w, d);
    }

    /**
     * Prints the Matrix to the specified output.
     * @param output PrintWriter to write to.
     * @param w Width for each cell.
     * @param d Number of decimal points for each cell.
     */
    public void print(PrintWriter output, int w, int d) {
        DecimalFormat format = new DecimalFormat();
        format.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
        format.setMinimumIntegerDigits(1);
        format.setMaximumFractionDigits(d);
        format.setMinimumFractionDigits(d);
        format.setGroupingUsed(false);
        print(output, format, w + 2);
    }

    /**
     * Prints the Matrix to the specified output.
     * @param output PrintWriter to write to.
     * @param format Format to write numbers.
     * @param width The width to pad each cell to.
     */
    public void print(PrintWriter output, NumberFormat format, int width) {
        output.println(getClass().toString() + '@' + hashCode());
        for (int i = 0; i < m; i++) {
        	output.print('|');
            for (int j = 0; j < n; j++) {
                String s = format.format(getData(i, j));
                int padding = Math.max(1, width - s.length());
                for (int k = 0; k < padding; k++) {
                    output.print(' ');
                }
                output.print("(" + i + j + ") " + s);
                output.print('|');
            }
            output.println();
        }
        output.println();
    }
}
