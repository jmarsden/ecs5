/**
 * Copyright (C) 2015-2020 J.W.Marsden <j.w.marsden@gmail.com>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package co.cata.ds.graph;

import java.util.List;

/**
 *
 * @param <N>
 * @param <E>
 */
public interface EdgeGraph<N,E extends Edge<N>> {
    
    public boolean isDirected();

    public void setGraphDegree(int degree);
    
    public int getGraphDegree();
    
    public int getDegree(N n);
    
    public int hasEdge(N source, N target);
    
    public E getEdge(N source, N target);
    
    public List<E> getEdges(N source, N target);
    
    public List<E> getOutwardEdges(N n);
    
    public List<E> getInwardEdges(N n);
    
    public E createEdge(N source, N target);
    
}
