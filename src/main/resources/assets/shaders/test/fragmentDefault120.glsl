#version 120

in vec4 ex_Color;
vec4 out_Color;
    
void main(void)
{
    out_Color = ex_Color;
}