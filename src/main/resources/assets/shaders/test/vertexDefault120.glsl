#version 120
 
in vec4 in_Position;
in vec4 in_Color;
in vec2 in_TextureCoord;

varying vec4 ex_Color;
varying vec2 pass_TextureCoord;
 
void main(void)
{
    gl_Position = in_Position;
    ex_Color = in_Color;
    pass_TextureCoord = in_TextureCoord;
}