#version 130

uniform sampler2D texture_diffuse;

in vec2 pass_TextureCoord;
in vec4 pass_Color;

out vec4 out_Color;

void main(void) {
    out_Color = pass_Color * texture2D(texture_diffuse, pass_TextureCoord);
}